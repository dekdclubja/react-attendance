# react-attendance

## Install

```bash
npm install --save react-attendance
```

## Usage
```bash
import Attendance from 'react-attendance';

const App = () => (
  <div>
    <Attendance
      hostId={'host id [require]'}
      staffId={'staff or editor [require]'}
      year={'year [require]'}
      semester={'semester [require]'}
      educationClass={'education string [require]'}
      room={'room no [require]'}
      month={'month no [require]'}
      token={'token id [require]'}
      onStatusChanged={'handle on attend changed [optional]'} />
  </div>
);

```

## License
Inforvation