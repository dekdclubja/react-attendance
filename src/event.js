import Core, { version } from './core'
const serviceUrl = 'https://inforvation.systems/mongodb/obec/'
const serverTimeUrl = 'https://inforvation.systems/servertime'

var parent = null
var serverTime = new Date()
var docs = null
var students = null
var holiday = null

const _fetch = (method, url, data, callback) => {
  var options = {
    method,
    headers: { 'Content-Type': 'application/json', 'Authorization': 'JWT ' + parent.props.token },
  }
  if (method === 'POST') options.body = JSON.stringify(data)
  fetch(url, options)
    .then(response => response.json())
    .then(data => {
      if (data.ok === false) return callback(true, data.message)
      callback(null, data)
    })
    .catch(err => callback(true, err))
}

const getHoliday = (callback) => {
  _fetch('GET', serverTimeUrl, null, (err, msg) => {
    if (err) return callback(err, msg)
    serverTime = new Date(msg)
    const qry = {
      query: {
        year: serverTime.getFullYear()
      },
      limit: 1
    }
    _fetch('POST', serviceUrl + 'publicholiday/query', qry, callback)
  })
}

const getStudent = (callback) => {
  const qry = {
    query: {
      hostid: parent.props.hostId,
      class: parent.props.educationClass,
      room: parent.props.room
    },
    projection: { title: 1, name: 1, lastname: 1, WelfareId: 1 },
    limit: 100
  }
  _fetch('POST', serviceUrl + 'student_data_db' + parent.props.year + '' + parent.props.semester + '/query', qry, callback)
}

const getDocument = (callback) => {
  const start = new Date(serverTime.getFullYear(), parent.props.month - 1, 1)
  const end = new Date(serverTime.getFullYear(), parent.props.month, 0)
  const qry = {
    query: {
      hostid: parent.props.hostId,
      year: parent.props.year,
      semester: parent.props.semester,
      educationclass: parent.props.educationClass,
      room: parent.props.room,
      recdate: { $gte: start.getTime(), $lte: end.getTime() }
    },
    limit: 31
  }
  _fetch('POST', serviceUrl + 'morning_attendance/query', qry, callback)
}

const _docToTable = (students, docs, total, month) => {
  var table = []
  const days = new Date(serverTime.getFullYear(), month, 0).getDate()
  for (let student of students) {
    var row = [student._id, student.title + '' + student.name + ' ' + student.lastname]
    for (let i = 0; i < days; i++) {
      const searchDay = docs.filter(x => (new Date(x.recdate)).getDate() === (i + 1))
      if (searchDay.length === 0) {
        row.push(undefined)
      }
      else {
        const searchStudent = searchDay[0].attend.filter(x => x.cid === student._id)
        row.push(searchStudent.length === 0 ? '' : Core.textToAttend(searchStudent[0].desc))
      }
    }
    const absent = student['ขาดเรียน'] || 0
    const leave = student['ลากิจ'] || 0
    const sick = student['ลาป่วย'] || 0
    const late = student['มาสาย'] || 0
    row.push(total - (absent + leave + sick + late))
    row = row.concat([absent, leave, sick, late])
    table.push(row)
  }
  return table
}

const _getSummary = (callback) => {
  const query = {
    "aggregate": [
      {
        "$match": {
          "hostid": "SU2017A017",
          "year": 2017,
          "semester": 2,
          "educationclass": "ประถมศึกษาปีที่ 1",
          "room": "1",
          "recdate": { "$gte": 1514739600000, "$lte": 1517331600000 }
        }
      },
      {
        "$group": {
          "_id": { "cid": "$attend.cid", "desc": "$attend.desc" },
          "count": { "$sum": 1 }
        }
      }
    ]
  }
  _fetch('POST', serviceUrl + 'morning_attendance/query', query, (err, msg) => {
    if (err) return callback(err, msg)
    const total = msg.reduce((total, obj) => {
      return total + obj.count
    }, 0)
    for (let student of students) {
      for (let doc of msg) {
        const index = doc._id.cid.findIndex(x => x === student._id)
        if (index > -1) {
          const value = student[doc._id.desc[index]]
          student[doc._id.desc[index]] = isNaN(Number(value)) ? doc.count : value + doc.count
        }
      }
    }
    callback(null, total)
  })
}

const onLoad = (_parent, month, callback) => {
  parent = _parent
  getHoliday((err, msg) => {
    if (err) return callback(err, msg)
    holiday = msg[0].publicholiday
    getStudent((err, msg) => {
      if (err) return callback(err, msg)
      msg.sort((a, b) => {
        var _a = a.title + ' ' + a.name + ' ' + a.lastname;
        var _b = b.title + ' ' + b.name + ' ' + b.lastname;
        return _a.localeCompare(_b)
      });
      students = msg
      getDocument((err, msg) => {
        docs = msg
        _getSummary((err, msg) => {
          callback(null, {
            timestamp: serverTime.getTime(),
            result: _docToTable(students, docs, msg, month),
            holiday: holiday
          })
        })
      })
    })
  })
}

const onChanged = (data, callback) => {
  const date = (new Date(data.date)).setHours(0, 0, 0, 0)
  var index = docs.findIndex(x => x.recdate === date)
  if (index > -1) {
    var doc = docs[index]
    doc.version = version
    doc.platform = 'web'
    doc.system = 'cct'
    doc.staffid = parent.props.staff
    var index = doc.attend.findIndex(x => x.cid === data.cid)
    if (index > -1) {
      data.value === undefined ? doc.attend.splice(index, 1) : doc.attend[index].desc = data.value
    }
    else if (data.value) {
      doc.attend.push({ cid: data.cid, desc: data.value })
    }
    _fetch('GET', serverTimeUrl, null, (err, msg) => {
      if (err) return callback(err, msg)
      doc.timestamp = msg
      _fetch('POST', serviceUrl + 'morning_attendance' + '/data/' + doc._id, doc, callback)
    })
  }
}

const onChecked = (data, callback) => {
  const date = (new Date(data.date)).setHours(0, 0, 0, 0)
  if (data.checked) {
    const qry = {
      query: {
        hostid: parent.props.hostId,
        year: parent.props.year,
        semester: parent.props.semester,
        educationclass: parent.props.educationClass,
        room: parent.props.room,
        recdate: date
      },
      limit: 1
    }
    _fetch('POST', serviceUrl + 'morning_attendance/query', qry, (err, msg) => {
      if (err) return callback(err, msg)
      if (msg.length > 0) {
        var index = docs.findIndex(x => x.recdate === date)
        if (index === -1) {
          docs.push(msg[0])
          parent.refresh(_docToTable(students, docs), () => { })
        }
      }
      else {
        _fetch('GET', serverTimeUrl, null, (err, msg) => {
          if (err) return callback(err, msg)
          const _data = {
            "hostid": parent.props.hostId,
            "year": parent.props.year,
            "semester": parent.props.semester,
            "recdate": date,
            "staffid": parent.props.staff,
            "educationclass": parent.props.educationClass,
            "room": parent.props.room,
            "timestamp": msg,
            "attend": []
          }
          _fetch('POST', serviceUrl + 'morning_attendance/data', _data, (err, msg) => {
            if (err) return callback(err, msg)
            _fetch('GET', serviceUrl + 'morning_attendance/data/' + msg.key, null, (err, msg) => {
              if (err) return callback(err, msg)
              docs.push(msg)
              callback(null, data)
            })
          })
        })
      }
    })
  }
  else {
    var index = docs.findIndex(x => x.recdate === date)
    if (index > -1) {
      _fetch('DELETE', serviceUrl + 'morning_attendance/data/' + docs[index]._id, null, (err, msg) => {
        if (err) return callback(err, msg)
        docs.splice(index, 1)
        callback(null, data)
      })
    }
  }
}

module.exports = {
  onLoad,
  onChanged,
  onChecked
}