import React, { Component } from 'react'
import HotTable from 'react-handsontable'
import Core, { months } from './core'
import { center4row, center, centerSmall, checkbox, alignLeft, alignRight, number } from './render'
import Event from './event'

/*
  props:
    // call callback to enable col after checked
    onChecked({date, checked, col}, callback) then callback({date, checked, col})
    onChanged({date, cid, value})
*/

class Attendance extends Component {
  constructor(props) {
    super(props);
    this.date = null
    this.holiday = []
    this.serverTime = null
    this.state = {
      settings: null
    }
  }

  componentDidMount() {
    this._start(this.props)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.hostId === this.props.hostId
      && nextProps.staffId === this.props.staffId
      && nextProps.year === this.props.year
      && nextProps.semester === this.props.semester
      && nextProps.educationClass === this.props.educationClass
      && nextProps.room === this.props.room
      && nextProps.month === this.props.month) return
    this._start(nextProps)
  }

  _start(props) {
    if (props.onStatusChanged) props.onStatusChanged('กำลังโหลดข้อมูล...')
    Event.onLoad(this, props.month, (err, msg) => {
      if (err) return this.onError(msg)
      if (props.onStatusChanged) props.onStatusChanged('กำลังประมวลผล...')
      var date = new Date(msg.timestamp)
      date.setDate(1)
      date.setMonth(props.month - 1)
      this.date = date.getTime()
      this.serverTime = msg.timestamp
      this.holiday = msg.holiday
      const settings = this._getSettings(msg.result)
      this.setState({ settings }, () => {
        const parent = document.querySelector('#hot').parentNode
        const table = document.querySelector('#hot > div > div > div > div > table')
        table.style.paddingLeft = (parent.offsetWidth - table.offsetWidth) / 2 + 'px'
        if (props.onStatusChanged) props.onStatusChanged('สำเร็จ')
        document.querySelector('#hotdescription').style.paddingLeft = (parent.offsetWidth - table.offsetWidth) + 'px'
      })
    })
  }

  onError(error) {
    console.log(error)
    if (this.props.onStatusChanged) this.props.onStatusChanged('เกิดข้อผิดพลาด! ' + error)
    if (this.props.onError) this.props.onError(error)
  }

  onChecked(e) {
    document.getElementById(e.target.id).disabled = true
    const date = new Date(this.date)
    const col = Number(e.target.id.replace('check', ''))
    date.setDate(col - 1)
    var result = {
      date,
      checked: e.target.checked,
      col: col,
      prev: []
    }
    if (!e.target.checked) {
      const data = this.state.settings.data
      var text = data[1][col] + ' ที่ ' + data[2][col] + ' ' + data[0][2];
      if (window.confirm('ต้องการลบข้อมูลของวัน ' + text + ' หรือไม่ ?')) {
        if (this.props.onStatusChanged) this.props.onStatusChanged('กำลังบันทึกข้อมูล...')
        var tmp = []
        for (let row of data) tmp.push(row[col])
        result.prev = tmp.slice(4)
        Event.onChecked(result, this._onCheckedCallback.bind(this))
      }
      else {
        e.target.checked = true
      }
    }
    else {
      if (this.props.onStatusChanged) this.props.onStatusChanged('กำลังบันทึกข้อมูล...')
      Event.onChecked(result, this._onCheckedCallback.bind(this))
    }
  }

  _onCheckedCallback(err, { checked, col, prev }) {
    document.getElementById('check' + col).disabled = false
    if (err) return this.onError(msg)
    if (this.props.onStatusChanged) this.props.onStatusChanged('กำลังประมวลผล...')
    var data = this.state.settings.data.slice(4)
    const date = new Date(this.date)
    const days = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate()
    for (let row of data) {
      row[col] = checked ? '' : undefined
      if (checked) {
        row[days + 2] = Number(row[days + 2]) + 1
      }
      else {
        var val = prev.shift()
        if (val === "" || val === undefined) row[days + 2] = Number(row[days + 2]) - 1
        else if (val === "ข") row[days + 3] = Number(row[days + 3]) - 1
        else if (val === "ล") row[days + 4] = Number(row[days + 4]) - 1
        else if (val === "ป") row[days + 5] = Number(row[days + 5]) - 1
        else if (val === "ส") row[days + 6] = Number(row[days + 6]) - 1
      }
    }
    this.setState({ settings: this._getSettings(data) }, () => {
      if (this.props.onStatusChanged) this.props.onStatusChanged('สำเร็จ')
    })
  }

  onChanged(row, col, value) {
    const date = new Date(this.date)
    date.setDate(col - 1)
    var data = this.state.settings.data
    var result = {
      date,
      cid: data[row][0],
      value: Core.attendToText(value)
    }
    if (this.props.onStatusChanged) this.props.onStatusChanged('กำลังบันทึกข้อมูล...')
    Event.onChanged(result, (err, msg) => {
      if (err) return this.onError(msg)
      if (this.props.onStatusChanged) this.props.onStatusChanged('สำเร็จ')
    })
  }

  _genHeader(dateTime) {
    var date = new Date(dateTime.getTime())
    const days = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate()
    var row1 = ['cid', 'ชื่อ-นามสกุล', months[date.getMonth()]]
    var row2 = ['', '']
    var row3 = ['', '']
    date.setDate(date.getDate() - date.getDate());
    for (let i = 1; i <= days; i++) {
      date.setDate(date.getDate() + 1);
      row1.push('')
      row2.push(Core.dayToText(date.getDay()))
      row3.push(i)
    }
    row1.pop()
    row1 = row1.concat(['มา', 'ขาด', 'ลา', 'ป่วย', 'สาย'])
    return [row1, row2, row3, row2]
  }

  _isHoliday(date) {
    for (let obj of this.holiday) {
      let d = new Date(obj.date)
      if (date.getDate() === d.getDate() && date.getMonth() === d.getMonth()) {
        return true
      }
    }
    return false
  }

  _renderCell(row, col, prop, days, row2, data) {
    var isHaveData = false
    for (let row of data) if (row[col] !== undefined) { isHaveData = true; break }
    var cellProperties = {}
    var isHoliday = false
    var _date = new Date(this.date)
    _date.setDate(col - 1)
    if (row < 4) { // header
      cellProperties.readOnly = true
      if (row !== 3) {
        if (col < 2 || col >= days + 2) { // spanRow = 4
          cellProperties.renderer = center4row
        }
        else {
          cellProperties.renderer = row2[col] === undefined ? centerSmall : center
        }
      }
      else {
        if (row2[col] === undefined) { // sunday and saturday
          cellProperties.renderer = center
        }
        else {
          if (_date.getTime() <= this.serverTime) {
            // check holiday
            if (this._isHoliday(_date)) {
              cellProperties.renderer = center
            }
            else {
              cellProperties.renderer = function (instance, td, row, col, prop, value, cellProperties) {
                checkbox(td, col, isHaveData, this.onChecked.bind(this))
              }.bind(this)
            }
          }
          else {
            cellProperties.renderer = center
          }
        }
      }
    }
    else {
      if (col < 2) {
        cellProperties.readOnly = true
        cellProperties.renderer = alignLeft
      }
      else if (col >= days + 2) {
        cellProperties.readOnly = true
        cellProperties.renderer = number
      }
      else {
        if (row2[col] === undefined || !isHaveData) {
          cellProperties.readOnly = true
          cellProperties.renderer = center
        }
        else {
          if (_date.getTime() <= this.serverTime) {
            // check holiday
            if (this._isHoliday(_date)) {
              cellProperties.readOnly = true
              cellProperties.renderer = center
            }
            else {
              cellProperties.type = 'dropdown';
              cellProperties.source = ['', 'ข', 'ล', 'ป', 'ส'];
            }
          }
          else {
            cellProperties.readOnly = true
            cellProperties.renderer = center
          }
        }
      }
    }
    return cellProperties
  }

  _mergeCell(date) {
    const days = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate()
    var mergeCells = [
      { row: 0, col: 0, rowspan: 4, colspan: 1 },
      { row: 0, col: 1, rowspan: 4, colspan: 1 },
      { row: 0, col: 2, rowspan: 1, colspan: days - 1 },
      { row: 0, col: days + 2, rowspan: 4, colspan: 1 },
      { row: 0, col: days + 3, rowspan: 4, colspan: 1 },
      { row: 0, col: days + 4, rowspan: 4, colspan: 1 },
      { row: 0, col: days + 5, rowspan: 4, colspan: 1 },
      { row: 0, col: days + 6, rowspan: 4, colspan: 1 },
    ]
    return mergeCells
  }

  _colWidths(days, row2) {
    var colWidth = [1, 180]
    for (let i = 1; i <= days; i++) {
      if (row2[i + 1] === undefined) {
        colWidth.push(25)
      }
      else {
        var _isHoliday = false
        _isHoliday ? colWidth.push(25) : colWidth.push(37)
      }
    }
    colWidth = colWidth.concat([40, 40, 40, 40, 40])
    return colWidth
  }

  _update(days, changes) {
    var data = this.state.settings.data
    if (changes[2] === "" || changes[2] === undefined) data[changes[0]][days + 2] -= 1
    else if (changes[2] === "ข") data[changes[0]][days + 3] -= 1
    else if (changes[2] === "ล") data[changes[0]][days + 4] -= 1
    else if (changes[2] === "ป") data[changes[0]][days + 5] -= 1
    else if (changes[2] === "ส") data[changes[0]][days + 6] -= 1

    if (changes[3] === "" || changes[3] === undefined) data[changes[0] - 4][days + 2] += 1
    else if (changes[3] === "ข") data[changes[0]][days + 3] += 1
    else if (changes[3] === "ล") data[changes[0]][days + 4] += 1
    else if (changes[3] === "ป") data[changes[0]][days + 5] += 1
    else if (changes[3] === "ส") data[changes[0]][days + 6] += 1
    this.setState({ data }, () => {
      this.onChanged(changes[0], changes[1], changes[3])
    })
  }

  _wrapDropdown() {
    setTimeout(() => {
      const all = document.querySelectorAll('div.ht_master.handsontable')
      if (all.length === 1) return
      const current = all[all.length - 1]
      const tds = current.querySelectorAll('table, th, td').forEach((_td) => {
        _td.style.backgroundColor = '#B3E5FC'
        _td.style.border = '1px solid #B3E5FC'
        _td.style.cursor = 'pointer'
      })
    }, 100)
  }

  refresh(data, callback) {
    var _data = this.state.settings.data.slice(0, 4)
    _data = _data.concat(data)
    this.setState({ settings: this._getSettings(data) }, () => {
      callback(null, true)
    })
  }

  _getSettings(result) {
    const date = new Date(this.date)
    const _data = this._genHeader(date)
    const days = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate()
    if (result) for (let d of result) _data.push(d)
    return {
      data: _data,
      minSpareCols: 0,
      minSpareRows: 0,
      maxRows: result.length + 4,
      cells: function (row, col, prop) {
        return this._renderCell(row, col, prop, days, _data[1], result);
      }.bind(this),
      mergeCells: this._mergeCell(date),
      colWidths: this._colWidths(days, _data[1]),
      afterChange: function (changes, source) {
        if (changes) if (changes[0]) {
          if (changes[0][2] === undefined && changes[0][3] === "") return
          if (Core.attendToText(changes[0][3]) === undefined && changes[0][3] !== '') return
          this._update(days, changes[0])
        }
      }.bind(this),
      afterOnCellMouseDown: function (e, { row, col }, td) {
        if (row > 3 && col > 1 && col <= days + 2 && _data[1][col] !== undefined) {
          this._wrapDropdown()
        }
      }.bind(this),
      afterBeginEditing: function (row, col) {
        if (row > 3 && col > 1 && col <= days + 2 && _data[1][col] !== undefined) {
          this._wrapDropdown()
        }
      }.bind(this)
    }
  }

  render() {
    if (!this.state.settings) return null
    const holiday = this.holiday.filter(x => (new Date(x.date)).getMonth() === this.props.month - 1)
    return (
      <div>
        <HotTable root="hot" settings={this.state.settings} />
        <div id="hotdescription">
          {
            holiday.map((item, i) => {
              const date = new Date(item.date)
              return <div key={i}>
                {date.getDate() + ' ' + months[date.getMonth()] + ' ' + (date.getFullYear() + 543) + ' ' + item.text}
              </div>
            })
          }
        </div>
      </div>
    )
  }
}
export default Attendance;