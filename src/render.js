const center4row = function (instance, td, row, col, prop, value, _cellProperties) {
  td.innerText = value || ''
  td.style.backgroundColor = '#EEEEEE'
  td.style.textAlign = 'center'
  td.style.paddingTop = '35px'
  td.style.color = '#757575'
  return td
}

const center = function (instance, td, row, col, prop, value, cellProperties) {
  td.innerText = value || ''
  td.style.backgroundColor = '#EEEEEE'
  td.style.textAlign = 'center'
  td.style.color = '#757575'
  return td
}

const centerSmall = function (instance, td, row, col, prop, value, cellProperties) {
  td.innerText = value || ''
  td.style.backgroundColor = '#EEEEEE'
  td.style.textAlign = 'center'
  td.style.color = '#757575'
  td.style.fontSize = '10px'
  return td
}

const checkbox = function (td, col, checked, onchange) {
  td.style.backgroundColor = '#FFFFFF'
  td.style.textAlign = 'center'
  var input = document.createElement('input')
  if (td.firstChild) input = td.firstChild
  input.type = 'checkbox'
  input.id = 'check' + col
  input.onchange = onchange
  input.checked = checked
  td.appendChild(input)
  return td
}

const alignLeft = function (instance, td, row, col, prop, value, cellProperties) {
  td.innerText = value || ''
  td.style.backgroundColor = '#EEEEEE'
  td.style.textAlign = 'left'
  td.style.color = '#757575'
  td.style.overflow = 'hidden'
  td.style.textOverflow = 'ellipsis'
  td.style.whiteSpace = 'nowrap'
  td.style.fontSize = '80%'
  return td
}

const alignRight = function (instance, td, row, col, prop, value, cellProperties) {
  td.innerText = value || ''
  td.style.backgroundColor = '#EEEEEE'
  td.style.textAlign = 'right'
  td.style.color = '#757575'
  return td
}

const number = function (instance, td, row, col, prop, value, cellProperties) {
  td.innerText = isNaN(Number(value)) ? 0 : Number(value)
  td.style.backgroundColor = '#EEEEEE'
  td.style.textAlign = 'right'
  td.style.color = '#757575'
  return td
}

module.exports = {
  center4row,
  center,
  centerSmall,
  checkbox,
  alignLeft,
  alignRight,
  number
}