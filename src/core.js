const version = '0.0.1'
const months = ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน ", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"]
const days = ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์"]

const _classMapping = {
  '4': 'ประถมศึกษาปีที่ 1',
  '1': 'อนุบาล 1',
  '2': 'อนุบาล 2',
  '3': 'อนุบาล 3',
  '5': 'ประถมศึกษาปีที่ 2',
  '6': 'ประถมศึกษาปีที่ 3',
  '7': 'ประถมศึกษาปีที่ 4',
  '8': 'ประถมศึกษาปีที่ 5',
  '9': 'ประถมศึกษาปีที่ 6',
  '10': 'มัธยมศึกษาปีที่ 1',
  '11': 'มัธยมศึกษาปีที่ 2',
  '12': 'มัธยมศึกษาปีที่ 3',
  '13': 'มัธยมศึกษาปีที่ 4',
  '14': 'มัธยมศึกษาปีที่ 5',
  '15': 'มัธยมศึกษาปีที่ 6'
}

const _attendMapping = {
  'ข': 'ขาดเรียน',
  'ล': 'ลากิจ',
  'ป': 'ลาป่วย',
  'ส': 'มาสาย'
}

const _dayToText = { 1: 'จ.', 2: 'อ.', 3: 'พ.', 4: 'พฤ.', 5: 'ศ.' }
const _dayToTextEng = { 'จ.': 'monday', 'อ.': 'tuesday', 'พ.': 'wednesday', 'พฤ.': 'thursday', 'ศ.': 'friday' }

const _searchObjectValue = (obj, text) => {
  var search = Object.keys(obj).filter(x => obj[x] === text)
  if (search.length === 0) return null
  return search[0]
}

const classToText = (id) => _classMapping[id]

const textToClass = (text) => _searchObjectValue(_classMapping, text)

const attendToText = (id) => _attendMapping[id]

const textToAttend = (text) => _searchObjectValue(_attendMapping, text)

const dayToText = (id) => _dayToText[id]

const textToDay = (text) => _searchObjectValue(_dayToText, text)

const dayToTextEng = (id) => _dayToTextEng[id]

const textToDayEng = (text) => _searchObjectValue(_dayToTextEng, text)

module.exports = {
  version,
  months,
  days,
  classToText,
  textToClass,
  attendToText,
  textToAttend,
  dayToText,
  dayToText,
  dayToTextEng,
  textToDayEng
}