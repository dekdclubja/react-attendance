import React, { Component } from 'react';
import './App.css';
import Attendance from 'react-attendance';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: 'status',
      month: 1,
      holiday: []
    }
  }

  _onStatusChanged = (text) => {
    this.setState({ status: 'status : ' + text })
  }

  _onMonthChanged = (e) => {
    this.setState({ month: Number(e.target.value) })
  }

  render() {
    var demoToken = '';
    return (
      <div className="App">
        <select onChange={this._onMonthChanged}>
          <option value="1">มกราคม</option>
          <option value="2">กุมภาพันธ์</option>
          <option value="3">มีนาคม</option>
          <option value="4">เมษายน</option>
        </select>
        <span className="App-Status">{this.state.status}</span>
        <Attendance
          hostId="SU2017A017"
          staffId={"userTest"}
          year={2017}
          semester={2}
          educationClass="ประถมศึกษาปีที่ 1"
          room="1"
          month={this.state.month}
          token={demoToken}
          onStatusChanged={this._onStatusChanged} />
      </div>
    );
  }
}

export default App;
